/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.portal.web.client;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import org.butor.utils.Message;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author asawan
 *
 */
public class PortalSession {
	private final Logger logger = Logger.getLogger(PortalSession.class.getName());
	private String host;
	private Map<String, String> secQA = new HashMap<String, String>();
	private String id;
	private String pwd;
	private boolean connected = false;
	private boolean connecting = false;
	private String lang = "fr";

	private Gson gson = new GsonBuilder().create();

	OkHttpClient httpClient = new OkHttpClient.Builder().cookieJar(new CookieJar() {
		private final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();

		@Override
		public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
			cookieStore.put(url.host(), cookies);
		}

		@Override
		public List<Cookie> loadForRequest(HttpUrl url) {
			List<Cookie> cookies = cookieStore.get(url.host());
			return cookies != null ? cookies : new ArrayList<Cookie>();
		}
	}).build();

	String sessionId = "session-" + UUID.randomUUID().toString();

	public PortalSession(Map<String, String> args) {
		secQA.put(Preconditions.checkNotNull(args.get("q1"), "q1"), Preconditions.checkNotNull(args.get("r1"), "r1"));
		secQA.put(Preconditions.checkNotNull(args.get("q2"), "q2"), Preconditions.checkNotNull(args.get("r2"), "r2"));
		secQA.put(Preconditions.checkNotNull(args.get("q3"), "q3"), Preconditions.checkNotNull(args.get("r3"), "r2"));
		id = Preconditions.checkNotNull(args.get("id"), "id");
		pwd = Preconditions.checkNotNull(args.get("pwd"), "pwd");
		host = Preconditions.checkNotNull(args.get("host"), "host");
	}

	private void signIn() {
		if (connecting) {
			return;
		}
		logger.info("-------------\nSigning in ...");
		connecting = true;
		try {
			List<Object> serviceArgs = new ArrayList<>();

			// login step1
			Map<String, String> p1 = new HashMap<>();
			p1.put("id", id);
			p1.put("pwd", pwd);
			serviceArgs.add(p1);
			logger.info("Login step 1/2 : ");
			CallResponse<Map<String, Object>> sr1 = this.internalCall("/sso/login.ajax", "signInS1", serviceArgs);
			if (sr1.getMessages().size() > 0 && sr1.getMessages().get(0).getId().equals("LOGIN_FAILED")) {
				throw new IOException("Login failed");
			}
			boolean twoStepsSignin = true;
			if (sr1.getData().size() > 0) {
				Map<String, Object> rec1 = sr1.getData().get(0);
				if (rec1.containsKey("twoStepsSignin")) {
					twoStepsSignin = ((Boolean) rec1.get("twoStepsSignin")).booleanValue();
				}
			}
			logger.info("OK");

			logger.info("2 steps login? " + twoStepsSignin);
			if (twoStepsSignin) {
				logger.info("Get login step2 info : ");
				serviceArgs.clear();

				CallResponse<String> sr2 = this.internalCall("/sso/login.ajax", "getS2Info", serviceArgs);
				// logger.info(sr);
				if (sr2.getMessages().size() > 0 && sr2.getMessages().get(0).getId().equals("LOGIN_FAILED")) {
					throw new IOException("Login failed");
				}
				String sq = sr2.getData().get(0);
				logger.info("Security question : " + sq);
				String sa = secQA.get(sq);

				serviceArgs.clear();
				p1.clear();
				p1.put("q", sq);
				p1.put("r", sa);
				serviceArgs.add(p1);

				// sign step 2
				logger.info("Login step 2/2 : ");
				sr2 = this.internalCall("/sso/login.ajax", "signInS2", serviceArgs);
				boolean loginSuccess = false;
				if (sr2.getMessages() != null) {
					for (Message msg : sr2.getMessages()) {
						if (msg.getId().equals("LOGIN_SUCCESS")) {
							loginSuccess = true;
							break;
						}
					}
				}
				if (!loginSuccess) {
					throw new IOException("Login failed!");
				}
				//String ssoId = sr2.getData().get(0);
				logger.info("OK");
				logger.info("Signed in successfuly\n-------------");
			}
			connected = true;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			connecting = false;
		}
	}

	public synchronized <T> CallResponse<T> call(String endpoint, String service, List<Object> serviceArgs)
			throws IOException {
		if (!connected) {
			signIn();
		}
		if (!connected) {
			throw new IOException("Failed to sign in!");
		}
		CallResponse<T> sr = this.internalCall(endpoint, service, serviceArgs);
		if (sr.getMessages() != null) {
			for (Message msg : sr.getMessages()) {
				if (msg.getId().equals("SESSION_TIMEDOUT")) {
					connected = false;
					sr = call(endpoint, service, serviceArgs);
					break;
				}
			}
		}
		return sr;
	}

	private <T> CallResponse<T> internalCall(String endpoint, String service, List<Object> serviceArgs) throws IOException {
		String jsonServiceArgs = gson.toJson(serviceArgs);
		String reqId = UUID.randomUUID().toString();
		RequestBody formBody = new FormBody.Builder().add("streaming", "false").add("service", service)
				.add("lang", lang).add("reqId", reqId).add("sessionId", sessionId).add("args", jsonServiceArgs).build();

		Request request = new Request.Builder().url(host + endpoint).post(formBody).build();
		Response response = httpClient.newCall(request).execute();
		if (!response.isSuccessful())
			throw new IOException("Unexpected code " + response);

		String json = response.body().string();
		
		Type type = new TypeToken<CallResponse<T>>() {}.getType();
		return gson.fromJson(json, type);
	}
}
