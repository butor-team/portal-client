/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.portal.web.client;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author asawan
 *
 */
public class PortalSessionSample {
	private final static Logger logger = Logger.getLogger(PortalSessionSample.class.getName());
	public static void main(String[] args) throws IOException {
		Map<String, String> sessionArgs = new HashMap<String, String>();
		sessionArgs.put("id", "<id>");
		sessionArgs.put("pwd", "<pwd>");

		sessionArgs.put("q1", "<q1>");
		sessionArgs.put("r1", "<r1>");
		sessionArgs.put("q2", "<q2>");
		sessionArgs.put("r2", "<r2>");
		sessionArgs.put("q3", "<q3>");
		sessionArgs.put("r3", "<r3>");

		sessionArgs.put("host", "http://localhost:8080");

		PortalSession session = new PortalSession(sessionArgs);
		
		// call authorised services
		// check systems services interfaces for available services, their args and return type
		
		// get my profile
		logger.info("======= Get user profile =======");
		List<Object> serviceArgs = new ArrayList<Object>();
		CallResponse<Map<String, Object>> sr1 = session.call("/portal.ajax", "getProfile", serviceArgs);
		Map<String, Object> rec1 = sr1.getData().get(0);
		String lang = (String)rec1.get("language");
		logger.info("user lang=" +lang);
		
		// get my authorised functions
		logger.info("======= Get user authorised functions =======");
		serviceArgs = new ArrayList<Object>();
		sr1 = session.call("/bsec/auth.ajax", "listAuthFunc", serviceArgs);
		for (Map<String, Object> rec : sr1.getData()) {
			logger.info("func=" +rec);
		}

		// get some public attribute sets
		logger.info("======= list attributes set =======");
		serviceArgs.clear();
		List<Object> aslp = new ArrayList<Object>();
		Map<String, String> p1 = new HashMap<String, String>();
		p1.clear();
		p1.put("id","lang");
		p1.put("lang","fr");
		aslp.add(p1);
		
		Map<String, String> p2 = new HashMap<String, String>();
		p2.put("id","auth-mode");
		p2.put("lang","fr");
		aslp.add(p2);
		
		serviceArgs.add(aslp);
		CallResponse<List<Map<String, Object>>> sr2 = session.call("/codeset.ajax", "getCodeSets", serviceArgs);
		for (List<Map<String, Object>> asl : sr2.getData()) {
			logger.info("Attr Set=" +asl);
		}
	}
}
